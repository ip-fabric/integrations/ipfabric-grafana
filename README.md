# IP Fabric Grafana Integration

## IP Fabric

IP Fabric is a vendor-neutral network assurance platform that automates the
holistic discovery, verification, visualization, and documentation of
large-scale enterprise networks, reducing the associated costs and required
resources whilst improving security and efficiency.

It supports your engineering and operations teams, underpinning migration and
transformation projects. IP Fabric will revolutionize how you approach network
visibility and assurance, security assurance, automation, multi-cloud
networking, and trouble resolution.

**Integrations or scripts should not be installed directly on the IP Fabric VM unless directly communicated from the
IP Fabric Support or Solution Architect teams.  Any action on the Command-Line Interface (CLI) using the root, osadmin,
or autoboss account may cause irreversible, detrimental changes to the product and can render the system unusable.**

## Project Description

This Grafana template file allows users to view IP Fabric information directly
in Grafana using an Infinity data source. This example includes a Snapshot
selector, Device Inventory list (limited to 5k devices), a copy of the IP Fabric
Dashboard with clickable links to drill down directly to the information, and
examples of widgets of the key data points your team may care about.

For further information please see the following resources:

- [Integrating IP Fabric with Grafana Blog Post](https://ipfabric.io/blog/integrating-ip-fabric-with-grafana)
- [#TechFabric 7 - Integrating IP Fabric with Grafana](https://www.youtube.com/live/GkTKg2JHEEE)

## Upgrade Notices

### IP Fabric 6.3.x Upgrade

**Corrected JSON file on 2024-04-22.**

An issue after upgrading to IP Fabric 6.3.x was found causing links to not work
correctly.  Please follow the below instructions to fix this issue:

1. Go to your IP Fabric Dashboard and click on the Settings gear icon
2. Navigate to the JSON Model
3. Copy and Paste the JSON model in your favorite text editor
4. Do a find and replace. Ensure you are using a “normal search” and not regex or something.
   1. Find `options={\"f\"`
   2. Replace `options={\"filters\"`
5. Copy the JSON back to Grafana and Save

## Requirements

If IP Fabric is isolated from Public Internet and you are using a Grafana Cloud
service please see [List of source IPs to add to your allowlist
](https://grafana.com/docs/grafana-cloud/reference/allow-list/).

## Infinity Data Source Installation

First we will installing and configuring the IP Fabric instance as an Infinity
Data Source in Grafana.

1. In Grafana navigate to `Connections > Add new connection`
2. Search for `Infinity`
3. You may be required to install the connector if not already installed.
4. Next we will configure the data source:
5. For the `Name` use the FQDN of your server as this is used as a variable in
   the template file. (i.e. `demo1.eu.ipfabric.io`)
6. Expand `Authentication`
   1. Set `Auth Type` to `API Key`
   2. Set `Key` to `X-API-Token`
   3. Set `Value` to an API Token
      - [IP Fabric API Token Documentation](https://docs.ipfabric.io/latest/IP_Fabric_Settings/integration/api_tokens/)
      - It is recommended to use
        a [Read Only Role](https://docs.ipfabric.io/latest/IP_Fabric_Settings/administration/roles/)
        for the token with access to only the required endpoints. Please reach out
        to your Solution Architect if you have questions configuring this.
   4. `Add to` should be preselected to `Header`
   5. Under `Allowed hosts` enter the FQDN with `https://` and press `Add`
   6. Press `Save and test`
   7. Optionally: Under the `Cache` tab you are able to enable caching which is
     recommended as IP Fabric is a Snapshot based appliance that does not have
     live data.
   8. Under TLS/SSL & Network Settings there is an option to skip TLS 
      verification if your IP Fabric instance does not have a trusted 
      certificate installed.

Example:

![data_source.png](docs/data_source.png)

Note:

This template file has been created to connect to multiple IP Fabric instances.
If you have more than one IPF instance follow the instructions above for all of
your instances.

## Grafana Dashboard Installation

In this section we will be importing the Dashboard template file into Grafana.

1. In Grafana navigate to `Dashboards`
2. Under `New` select `Import`
3. Copy the data from [grafana_model.json](grafana_model.json) and paste into
   the `Import via panel json`
4. Select `Upload`
5. Optionally: Change the name of and folder of the Dashboard to be imported
   into.
6. Select `Import`

## Grafana Dashboard Configuration

Finally, we will be making some configuration changes according to your
environment.

1. At the top of the Dashboard select the gear icon to access
   the `Dashboard settings`
2. Under `Variables` select the `instance` variable.
   - **Single IP Fabric Deployment** (Common to most customers)
     1. Change the `Show on dashboard` to `Nothing`
     2. Under `Instance name filter` enter a regex to filter out all Infinity
        Data Sources as `/FQDN/` (i.e. `/demo1.eu.ipfabric.io/`)
     3. The preview should update to show only the single data source and
        press `Apply`
   - **Multiple IP Fabric Deployment** (Not common)
     1. Leave the `Show on dashboard` to `Label and Value`
     2. Under `Instance name filter` enter a regex to filter out all Infinity
        Data Sources as `/FQDN|FQDN2|FQDN3|.../` (
        i.e. `/demo1.eu.ipfabric.io|demo1.us.ipfabric.io/`)
     3. The preview should update to show all the data sources you created and
        press `Apply`
3. Press `Save Dashboard` and `Close`

Once completed your Dashboard should load and look like below. At the top there
is a `snapshot` to change the selected IP Fabric Snapshot.  If you have multiple
IP Fabric instance there will also be a selector to change the data source.

![dashboard.png](docs/dashboard.png)

## Support

Please open an issue in GitLab for new features. If you are having issues with
installation or configuration please reach out to your Solution Architect.

